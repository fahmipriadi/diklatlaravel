<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DeviceElektronikRequest as StoreRequest;
use App\Http\Requests\DeviceElektronikRequest as UpdateRequest;

/**
 * Class DeviceElektronikCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DeviceElektronikCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\DeviceElektronik');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/deviceelektronik');
        $this->crud->setEntityNameStrings('deviceelektronik', 'device_elektroniks');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();
        
        $this->crud->addField([
            // 1-n relationship
            'label' => "Pilih Device", 
            'type' => "select",
            'name' => 'type', 
            'entity' => 'DeviceElektronikType', 
            'attribute' => "type", 
            'model' => "App\Models\DeviceElektronikType", 
        ]);

        // add asterisk for fields that are required in DeviceElektronikRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
