<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'wbabel@pln.co.id',
                'password' => '21232f297a57a5a743894a0e4a801fc3',
            ],
        ]);
    }
}
