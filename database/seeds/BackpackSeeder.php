<?php

use Illuminate\Database\Seeder;

class BackpackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@pln.co.id',
            'password' => '$2y$10$L9rzcSMOln9aN5rEcGHoduHiJ5zfvxEOVAkgeNbJvubTIxOuyqCRy',
        ]);
    }
}
